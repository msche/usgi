µSGi stands for micro Service Gateway initiative.

Framework by which µServices can be managed like OSGI bundles

Providing:

* Environment in which µServices can be deployed and un-deployed automatically without shutdown.
* Environment in which µServices are discovered automatically
* Environment in which µServices deployed in the same VM are communicating via direct java calls while if they are
distributed among multiple JVMs they are communicating via REST/SOAP.

## Faq
* If we would have 2 µServices which are both using same library but different version. Would that cause class path
collisions.

## Resources

* [Maven-junit4OSGI-plugin](http://felix.apache.org/documentation/subprojects/apache-felix-ipojo/apache-felix-ipojo-junit4osgi/apache-felix-ipojo-junit4osgi-maven.html)
* [Apache Felix](http://felix.apache.org/)
* [Apache CXF Distributed OSGI](https://cxf.apache.org/distributed-osgi.html)
