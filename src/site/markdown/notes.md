**Notes:**

* The topology activator is listening for bundle registration events. If such an event is received it will check whether services/interfaces are exposed and if so a endpoint will be created automatically and registered within the topology manager.
* Both the webconsole and dosgi are depending on jetty. For now we are using the felix jetty bundle but the eventual end product should be platform agnostic.
* Currently we are using OSGi for defining modules, its dependencies and the exposed services. Within Java 9 there will be JigSaw. It should be addressed how this can be supported also.
* Currently we are using Spring to define the module, we should investigate how we can extract info like defined services so that the bundle information can be generated automatically.